#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) 
{
	char buf[10], *ep;
	long a;
	while (scanf("%s", buf) == 1) {
		a = strtol(buf, &ep, 16);
		printf ("%c", a);
	}
	return 0;
}
